# Public gitlab repository VillaEtAl2021BullMathBiol

This repository provides Matlab files to perform simulations as described in
*C. Villa, M.A.J. Chaplain, A. Gerisch, T. Lorenzi.
Mechanical models of pattern and form in biological tissues: the role of stress-strain constitutive equations.
Bullettin of Mathematical Biology, volume 83, issue 7, article 80. (2021)*,
DOI: https://doi.org/10.1007/s11538-021-00912-5 .
For details we refer the interested reader to this publication and its supplementary material.

If you use this software in your work then please cite the above named paper.

## Copyright notice

VillaEtAl2021BullMathBio: simulate mechanical models of pattern formation   
Copyright (C) 2021 C. Villa and A. Gerisch

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
